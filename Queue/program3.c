/* Implementing Queue by using Linked list  second approach */

#include <stdio.h>
#include <stdlib.h>

typedef struct Queue{
	int data;
	struct Queue *next;
}node;

node *front=NULL;
node *rear=NULL;
int flag=0;


node *createnode(){
	node * newnode =(node *)malloc(sizeof(node));
	if(newnode == NULL){
		printf("Memmory Full\n");
		exit(0);
	}

	printf("Enter Data :\n");
	scanf("%d",&newnode->data);

	newnode ->next=NULL;
	return newnode;
}
void enqueue(){
		node * newnode=createnode();

		if(front==NULL){
			front=newnode;
			rear=newnode;
		}else{
			rear->next =newnode;
			rear=newnode;

		}
}



int dequeue(){
	if( front == NULL){
		flag =0;
		return -1;
	}else{
		flag =1;
		if(front->next == NULL){
			int data=front->data;
			free(front);
			front=NULL;
			return data;
		}else{
			node * temp=front;
			int data=temp->data;
			front =front->next;
			free(temp);
			return data;
		}
	}
}
int frontt(){
	if( front == NULL){
		flag =0;
		return -1;
	}else{
		flag==1;
		return front->data;
	}
}

int printQ(){
	if(front == NULL){
		return -1;
	}else{
		node * temp=front;
		while( temp->next !=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
		return 0;
	}
}

void main(){
	char ch;
	do{
		printf(" 1. enqueue\n 2.dequeue\n 3.frontt\n 4.printQueue\n");

		int choice;
		printf("Enter your choice :\n");
		scanf("%d",&choice);

		switch(choice){
			case 1:
				enqueue();
				break;
			case 2:
				{
					int data=dequeue();
					if(flag==1){
						printf("%d is dequeue\n ",data);
					}else{
						printf("Queue Underflow\n");
					}

				}
				break;
			case 3:
				{
					int data=frontt();
					if(flag !=0){
						printf("%d is front\n",data);
					}else{
						printf("Queue is Empty\n");
					}
				}
				break;
			  case 4:
                                {
                                        int data=printQ();
                                        if(data ==-1){
                                                printf("Queue is Empty\n");
					}
                                }
                                break;

			default :
				printf("Wrong Choice\n");
				break;
		}
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&ch);
	}while((ch=='Y') || (ch == 'y'));
}
