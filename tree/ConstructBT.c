#include <stdio.h>
#include<stdlib.h>

struct TreeNode{

	int data;
	struct TreeNode * left;
	struct TreeNode * right;
};

struct TreeNode * constructBT(int inorder[],int preorder[],int inStart,int inEnd,int preStart,int preEnd){

	if(inStart >inEnd){
		return NULL;
	}else{

		int rootdata=preorder[preStart];

		struct TreeNode*temp=malloc(sizeof(struct TreeNode));

		temp->data=rootdata;
		int idx;
		for(idx=inStart;idx<=inEnd;idx++){
			if(inorder[idx]==rootdata){
				break;
			}
		}
		int Llen=idx-inStart;

		temp->left=constructBT(inorder,preorder,inStart,idx-1,preStart+1,preStart+Llen);
		temp->right=constructBT(inorder,preorder,idx+1,inEnd,preStart+Llen+1,preEnd);
		return temp;
	}
}

void inorder(struct TreeNode * root){

	if(root==NULL){
		return;
	}
	inorder(root->left);
	printf("%d ",root->data);
	inorder(root->right);
}
void preorder(struct TreeNode * root){

	if(root==NULL){
		return;
	}
	printf("%d ",root->data);
	preorder(root->left);
	preorder(root->right);
}
void main(){

	int Inorder[]={2,5,4,1,6,3,8,7};
	int Preorder[]={1,2,4,5,3,6,7,8};

	int inStart=0;
	int inEnd=7;
	int preStart=0;
	int preEnd=7;

	struct TreeNode * root=constructBT(Inorder,Preorder,inStart,inEnd,preStart,preEnd);
	
	inorder(root);
	printf("\n");
	preorder(root);
	printf("\n");
}

		
