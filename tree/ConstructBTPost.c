#include <stdio.h>
#include<stdlib.h>

struct TreeNode{

	int data;
	struct TreeNode * left;
	struct TreeNode * right;
};

struct TreeNode * constructBT(int inorder[],int postorder[],int inStart,int inEnd,int postStart,int postEnd){

	if(inStart >inEnd){
		return NULL;
	}else{

		int rootdata=postorder[postEnd];

		struct TreeNode*temp=malloc(sizeof(struct TreeNode));

		temp->data=rootdata;
		int idx;
		for(idx=inStart;idx<=inEnd;idx++){
			if(inorder[idx]==rootdata){
				break;
			}
		}
		int Llen=idx-inStart;

		temp->left=constructBT(inorder,postorder,inStart,idx-1,postStart,postStart+Llen-1);
		temp->right=constructBT(inorder,postorder,idx+1,inEnd,postStart+Llen,postEnd-1);
		return temp;
	}
}

void inorder(struct TreeNode * root){

	if(root==NULL){
		return;
	}
	inorder(root->left);
	printf("%d ",root->data);
	inorder(root->right);
}
void preorder(struct TreeNode * root){

	if(root==NULL){
		return;
	}
	printf("%d ",root->data);
	preorder(root->left);
	preorder(root->right);
}

void postOrder(struct TreeNode * root){

	if(root==NULL){
		return;
	}
	postOrder(root->left);
	postOrder(root->right);
	printf("%d ",root->data);
}
void main(){

	int Inorder[]={2,5,4,1,6,3,8,7};
	int Postorder[]={5,4,2,6,8,7,3,1};

	int inStart=0;
	int inEnd=7;
	int postStart=0;
	int postEnd=7;

	struct TreeNode * root=constructBT(Inorder,Postorder,inStart,inEnd,postStart,postEnd);
	
	inorder(root);
	printf("\n");
	preorder(root);
	printf("\n");
	postOrder(root);
	printf("\n");
}

		
