#include <stdio.h>
#include <stdlib.h>
#include<stdbool.h>

struct TreeNode{
	int data;
	struct TreeNode *left;
	struct TreeNode *right;
};

struct StackFrame{
	struct TreeNode * bit;
	struct StackFrame * next;
};

struct StackFrame * top=NULL;
struct TreeNode* createNode(int level){
	level=level+1;
	struct TreeNode*newnode=(struct TreeNode*)malloc (sizeof(struct TreeNode));
	
	printf("Enter Data:\n");
	scanf("%d",&(newnode->data));
	char ch;
	
	getchar();
	printf("want to create left subtree for root node %d ?\n",level);
	scanf("%c",&ch);

	if(ch=='y' || ch=='Y'){
		newnode->left=createNode(level);
	}else{
		newnode->left=NULL;
	}
	getchar();
	printf("Want to create right subtree for root nade %d ?\n",level);
	scanf("%c",&ch);
	if(ch=='y' || ch=='Y'){
		newnode->right=createNode(level);
	}else{
		newnode->right=NULL;
	}
	return newnode;

}

void preorder(struct TreeNode * root){
	if(root==NULL){
		return;
	}
	printf("%d ",root->data);
	preorder(root->left);
	preorder(root->right);
}
void inorder(struct TreeNode * root){
	if(root==NULL){
		return;
	}
	inorder(root->left);
	printf("%d ",root->data);
	inorder(root->right);
}
void postorder(struct TreeNode * root){
	if(root==NULL){
		return;
	}
	
	postorder(root->left);
	postorder(root->right);
	printf("%d ",root->data);
}
bool isEmpty(){

	if(top==NULL)
		return true;
	else
		return false;
}

void push(struct TreeNode* temp){
	struct StackFrame *newnode=(struct StackFrame *)malloc(sizeof(struct StackFrame));

	newnode->next=top;
	newnode->bit=temp;
	top=newnode;
}

struct TreeNode* pop(){
	struct StackFrame * item=top;
	top=top->next;
	struct TreeNode* temp1=item->bit;
	free(item);
	return temp1;
}

void iterativeInorder(struct TreeNode * root){
	struct TreeNode * temp=root;
	while((!isEmpty()) || temp !=NULL){

		if(temp != NULL){
			push(temp);
			temp=temp->left;
		}else{
			temp=pop();
			printf("%d ",temp->data);
			temp=temp->right;
		}
	}

}

void printTree(struct TreeNode*root){

	char ch;
	do{
		printf("1.Preorder\n");
		printf("2.Inorder\n");
		printf("3.Postorder\n");
		printf("4.IterativeInorder\n");

		int choice;

		printf("Enter choice:\n");
		scanf("%d",&choice);

		switch(choice){

			case 1:
				preorder(root);
				break;
			case 2:
				inorder(root);
				break;
			case 3:
				postorder(root);
				break;
			case 4:
				iterativeInorder(root);
				break;
		}
		getchar();
		printf("Do you want to Continue?\n");
		scanf("%c",&ch);
	}while(ch=='y' || ch=='Y');
}


void main(){

	struct TreeNode * root=(struct TreeNode*)malloc(sizeof(struct TreeNode));
	
	printf("Enter Data:\n");
	scanf("%d",&(root->data));
	char ch;
	
	getchar();
	printf("want to create left subtree for root node?\n");
	scanf("%c",&ch);

	if(ch=='y' || ch=='Y'){
		root->left=createNode(0);
	}else{
		root->left=NULL;
	}
	getchar();
	printf("Want to create right subtree for root nade?\n");
	scanf("%c",&ch);
	if(ch=='y' || ch=='Y'){
		root->right=createNode(0);
	}else{
		root->left=NULL;
	}
	printTree(root);
	iterativeInorder(root);
}
