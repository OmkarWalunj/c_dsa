#include <stdio.h>
#include <stdlib.h>
#include<stdbool.h>

struct TreeNode{
	int data;
	struct TreeNode *left;
	struct TreeNode *right;
};

struct Queue{

	struct TreeNode*btNode;
	struct Queue*next;
};
struct Queue * front =NULL;
struct Queue * rear =NULL;

struct TreeNode* createNode(int level){
	level=level+1;
	struct TreeNode*newnode=(struct TreeNode*)malloc (sizeof(struct TreeNode));
	
	printf("Enter Data:\n");
	scanf("%d",&(newnode->data));
	char ch;
	
	getchar();
	printf("want to create left subtree for root node %d ?\n",level);
	scanf("%c",&ch);

	if(ch=='y' || ch=='Y'){
		newnode->left=createNode(level);
	}else{
		newnode->left=NULL;
	}
	getchar();
	printf("Want to create right subtree for root nade %d ?\n",level);
	scanf("%c",&ch);
	if(ch=='y' || ch=='Y'){
		newnode->right=createNode(level);
	}else{
		newnode->right=NULL;
	}
	return newnode;

}


bool isEmpty(){

	if(front==NULL )
		return true;
	else
		return false;
}

void enqueue(struct TreeNode * root){
	
	struct Queue * newnode=(struct Queue*)malloc(sizeof(struct Queue));

	newnode->btNode=root;
	newnode->next=NULL;

	if(isEmpty()){
		front=rear=newnode;
	}else{
		rear->next=newnode;
		rear=newnode;
	}

}

struct TreeNode* dequeue(){
	if(isEmpty()){
		printf("Queue is Empty\n");
	}else{
		struct Queue *temp=front;

		struct TreeNode * item=temp->btNode;
		if(front ==rear){
			front=rear=NULL;
		}else{
			front =front->next;
		}
		free(temp);
		return item;
	}
}

int size(){
	
	struct Queue * temp=front;
	int count=0;
	while(temp != rear){
		count++;
		temp=temp->next;
	}
	return count+1;
}
void levelOrder(struct TreeNode*root){

	struct TreeNode *temp=root;

	enqueue(root);

	while(!isEmpty()){
	        int sz=size();
		for(int i=0;i<sz;i++){
	        	temp=dequeue();
			printf("%d ",temp->data);
			if(temp->left != NULL){
				enqueue(temp->left);
			}
			if(temp->right !=NULL){
				enqueue(temp->right);
			}
		}
		printf("\n");		
	}
}
void printTree(struct TreeNode*root){

	char ch;
	do{
		printf("1.Preorder\n");

		int choice;

		printf("Enter choice:\n");
		scanf("%d",&choice);

		switch(choice){

			case 1:
				levelOrder(root);
				break;
				
		}
		getchar();
		printf("Do you want to Continue?\n");
		scanf("%c",&ch);
	}while(ch=='y' || ch=='Y');
}


void main(){

	struct TreeNode * root=(struct TreeNode*)malloc(sizeof(struct TreeNode));
	
	printf("Enter Data:\n");
	scanf("%d",&(root->data));
	char ch;
	
	getchar();
	printf("want to create left subtree for root node?\n");
	scanf("%c",&ch);

	if(ch=='y' || ch=='Y'){
		root->left=createNode(0);
	}else{
		root->left=NULL;
	}
	getchar();
	printf("Want to create right subtree for root nade?\n");
	scanf("%c",&ch);
	if(ch=='y' || ch=='Y'){
		root->right=createNode(0);
	}else{
		root->left=NULL;
	}
	printTree(root);

}
