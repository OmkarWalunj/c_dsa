#include <stdio.h>
#include <stdlib.h>

struct node{

	int data;
	struct node* left;
	struct node* right;
};
struct node * head=NULL;

struct node* createnode(struct node * root){
	if(head==NULL){
		head=root;
	}
	
	char ch;
	printf("Enter data On left side of %d (y||n)\n",root->data);
	scanf(" %c",&ch);
	if(ch=='y'){
		struct node * newnode=(struct node*)malloc(sizeof(struct node));
		printf("Enter data:\n");
		scanf("%d",&newnode->data);
		root->left=newnode;
		printf("%d data added left side of %d\n",root->left->data,root->data);
		createnode(newnode);
	}else{
		 root->left=NULL;
	}

	char ch1;
	printf("Enter data On right side of %d (y||n)\n",root->data);
	scanf(" %c",&ch1);
	if(ch1=='y'){
		struct node * newnode=(struct node*)malloc(sizeof(struct node));
		printf("Enter data:\n");
		scanf("%d",&newnode->data);
		 root->right=newnode;

		 printf("%d data added right side of %d\n",root->right->data,root->data);
		 createnode(newnode);
	}else{
		 return root->right=NULL;
	}

}

void printtree(struct node * root){
	if(root !=NULL){
		printf("%d ",root->data);
		printtree(root->left);
		printtree(root->right);
	}
}

void main(){

	struct node * newnode=(struct node*)malloc(sizeof(struct node));
	
	printf("Enter root data:\n");
	scanf("%d",&newnode->data);

	newnode->left=NULL;
	newnode->right=NULL;

	createnode(newnode);
	//printf("%d\n",head->left->data);
	printtree(head);
	printf("\n");

}
