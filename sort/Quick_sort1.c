//Quick sort(Haorse approach) pivert is 1st elements

//sort in given array;

#include <stdio.h>
int partition(int*,int,int);
void Quick_sort(int * arr,int start,int end){

	if(start<end){

		int pivert=partition(arr,start,end);

		Quick_sort(arr,start,pivert);
		Quick_sort(arr,pivert+1,end);
	}
}
void swap(int* ptr1,int* ptr2){
	int temp=*ptr1;
	*ptr1=*ptr2;
	*ptr2=temp;
}

int partition(int arr[],int start,int end){
	int pivert=arr[start];
	int i=start-1;
	int j=end+1;
	while(1){

		do{
			i++;
		}while(arr[i] < pivert);

		do{
			j--;
		}while(arr[j]>pivert);

		if(i>=j){
			return j;
		}
		swap(&arr[i],&arr[j]);
	}

}

void main(){

	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array Elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	Quick_sort(arr,0,size-1);
	printf("Sorted Array :\n");
	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
