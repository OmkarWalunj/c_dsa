//Quick sort

//mergesort in given array;

#include <stdio.h>
int partition(int*,int,int);
void Quick_sort(int * arr,int start,int end){

	if(start<end){

		int pivert=partition(arr,start,end);

		Quick_sort(arr,start,pivert-1);
		Quick_sort(arr,pivert+1,end);
	}
}

int partition(int arr[],int start,int end){
	int pivert=arr[end];
	int itr=start;
	int k=0;

	int arr1[(end-start)+1];
	for(int i=start;i<end;i++){
		if(arr[i]<pivert){
			arr1[k++]=arr[i];
			itr++;
		}
	}
	int pos=itr;
	arr1[k++]=pivert;

	for(int j=0;j<end;j++){
		if(arr[j]>pivert){
			arr1[k]=arr[j];
			k++;
		}
	}
	for(int i=start;i<=end;i++){
		arr[i]=arr1[i-start];
	}
	return pos;

}

void main(){

	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array Elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	Quick_sort(arr,0,size-1);
	printf("Sorted Array :\n");
	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
