//Insertion_sort
/* Given an array arr[] consisting of n positive integer the task is to sort the array such that-
 * all even number must come before alll odd numbers.
 * all even numbers divisible by 5 must come first then even number not divisible by 5.
 * if two even number are  not divisible by 5 then the number having a genter index int he array will come first.
 */

#include <stdio.h>

void Insertion_sort(int * arr,int size){

	for(int i=1;i<size;i++){
		int val=arr[i];
		int j=i-1;
		for(;j>=0 && (arr[j]<val) && (val % 2==0) && (val %5==0);j--){
			arr[j+1]=arr[j];
		}
		arr[j+1]=val;
	}
}
void main(){

	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];
	printf("ENter ARRaY Elements;\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	Insertion_sort(arr,size);
	printf("Sorted arr :\n");

	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
