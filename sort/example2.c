/* subarray with given sum
 * -Given an array arr[] of non-negative integer and an integer sum find a subarray that adds to a given sum.
 *   Note: there may be more than one subarray with sum as the given sum first such subarray.
 */

#include <stdio.h>

int subarr(int * arr,int arr1[],int size,int sum){

	for(int i=0;i<size;i++){
		if(arr[i]==sum){
			arr1[0]=i;
			return 0;
		}
		int sum1=0;
		for(int j=i;j<size;j++){	
			sum1=sum1+arr[j];
			if(sum1>sum){
				break;
			}
			if(sum==sum1){
				arr1[0]=i;
				arr1[1]=j;
				return 0;
			}
			//sum1=sum1+arr[j];
		}
	}
	return -1;
}
void main(){
	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];
	int arr1[2];
	
	printf("Enter Array Elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int sum;
	printf("Enter Sum:\n");
	scanf("%d",&sum);

	int num=subarr(arr,arr1,size,sum);
	
	if(num == -1){
		printf("No subarray found\n");
	}else{
		printf("Sum of elements between indices %d and %d\n ",arr1[0],arr1[1]);
	}
}
