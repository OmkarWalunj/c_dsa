//Insertion_sort

#include <stdio.h>

void Insertion_sort(int * arr,int size){

	for(int i=1;i<size;i++){
		int val=arr[i];
		int j=i-1;
		for(;j>=0 && (arr[j]>val);j--){
			arr[j+1]=arr[j];
		}
		arr[j+1]=val;
	}
}
void main(){

	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];
	printf("ENter ARRaY Elements;\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	Insertion_sort(arr,size);
	printf("Sorted arr :\n");

	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
