//Bubble_sort in linked list

#include <stdio.h>
#include <stdlib.h>

typedef struct sort{

	int no;
	struct sort * next;
}sort;

sort * head=NULL;

struct sort * createnode(){

	sort *newnode=(sort*)malloc(sizeof(sort));

	printf("Enter Number :\n");
	scanf("%d",&newnode->no);

	newnode->next=NULL;

	return newnode;
}

void addnode(){
	sort * newnode=createnode();
	if(head ==NULL){
		head=newnode;
	}else{
		sort * temp=head;
		while(temp->next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}
int countnode(){
	sort * temp=head;
	int count=0;
	while(temp !=NULL){
		count++;
		temp= temp->next;
	}
	return count;
}

void printLL(){

	if(head == NULL){
		printf("Linked list is empty\n");
	}else{
		sort * temp=head;
		while(temp->next != NULL){
			printf("|%d|->",temp->no);
			temp=temp->next;
		}

		printf("|%d|\n",temp->no);
	}
}

void sorting(){
	int num =countnode();
	int i=0;
	while(i<num){
		sort* temp1=head;
		int j=0;
		while(j < num-i-1){
			if(temp1->no > temp1->next->no){
				int num1=temp1->no;
				temp1->no = temp1->next->no;
				temp1->next->no=num1;
			}
			temp1=temp1->next;
			j++;
		}
		i++;
	}
}
void main(){

	int size;
	printf("Enter size of Linked list:\n");
	scanf("%d",&size);

	for(int i=0;i<size;i++){
		addnode();
	}
	printLL();
	sorting();
	printLL();
}

