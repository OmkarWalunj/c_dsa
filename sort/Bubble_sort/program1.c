//Bubble sort

#include <stdio.h>

void Bubble_sort(int * arr,int size){

	for(int i=0;i<size-1;i++){
		for(int j=0;j<size-i-1;j++){
			if(arr[j]>arr[j+1]){
				int temp=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=temp;
			}
		}
	}
}

void main(){

	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];
	printf("ENter ARRaY Elements;\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	Bubble_sort(arr,size);
	printf("Sorted arr :\n");

	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
