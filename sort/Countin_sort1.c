// Counting Sort Negative Number

#include <stdio.h>

void Counting(int * arr,int size){
		//Find largest Element
		int max=arr[0];
		int min=arr[0];

		for(int i=1;i<size;i++){
			if(max<arr[i]){
				max=arr[i];
			}
			if(min>arr[i]){
				min=arr[i];
			}
		}
		for(int i=0;i<size;i++){
			arr[i]=arr[i]+max;
		}
		// take count array max+1 initialize all zero
		int count[max+max+1];
		
		for(int i=0;i<=(2*max);i++){
			count[i]=0;
		}
		for(int i=0;i<size;i++){
			count[arr[i]]++;
		}
		//prefixcount
		printf("PrefixCount :\n");
		for(int i=1;i<=(max*2);i++){
			count[i]=count[i-1]+count[i];
		}
		for(int i=0;i<=(max*2);i++){
			printf("%d ", count[i]);
		}
		printf("\n");
		//take output array
		int output[size];
		//for stable array
		for(int i=size-1;i>=0;i--){
			output[count[arr[i]]-1]=arr[i];
			count[arr[i]]--;
		}
		printf("Output array:");
		for(int i=0;i<size;i++){
			printf("%d ",output[i]);
			arr[i]=output[i]-max;
		}
		printf("\n");

}

void main(){

	int size;
	printf("Enter Size of array:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array Elements:\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	Counting(arr,size);
	printf("Sorted array:\n");
	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}


