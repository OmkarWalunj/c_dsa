//mergesort in given array;

#include <stdio.h>
void sorting(int*,int,int,int);
void merge(int * arr,int start,int end){

	if(start<end){

		int mid=(start+end)/2;

		merge(arr,start,mid);
		merge(arr,mid+1,end);
		sorting(arr,start,mid,end);
	}
}

void sorting(int arr[],int start,int mid,int end){
	int ele1=mid-start+1;
	int ele2=end-mid;

	int arr1[ele1];
	int arr2[ele2];

	for(int i=0;i<ele1;i++){
		arr1[i]=arr[start+i];
	}
	for(int j=0;j<ele2;j++){
		arr2[j]=arr[mid+1+j];
	}

	int itr1=0;
	int itr2=0;
	int itr3=start;

	while(itr1<ele1 && itr2<ele2){

		if(arr1[itr1] > arr2[itr2]){
			arr[itr3]=arr2[itr2];
			itr2++;
		}else{
			arr[itr3]=arr1[itr1];
			itr1++;
		}
		itr3++;
	}

	while(itr1<ele1){
		arr[itr3]=arr1[itr1];
		itr3++;
		itr1++;
	}
	while(itr2<ele2){
		arr[itr3]=arr2[itr2];
		itr2++;
		itr3++;
	}
}

void main(){

	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array Elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	merge(arr,0,size-1);
	printf("Sorted Array :\n");
	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
