// Counting Sort

#include <stdio.h>

void Counting(int * arr,int size){
		//Find largest Element
		int max=arr[0];

		for(int i=1;i<size;i++){
			if(max<arr[i]){
				max=arr[i];
			}
		}
		// take count array max+1 initialize all zero
		int count[max+1];
		for(int i=0;i<=max;i++){
			count[i]=0;
		}
		for(int i=0;i<=max;i++){
			count[arr[i]]++;
		}
		//prefixcount
		for(int i=1;i<size;i++){
			count[i]=count[i-1]+count[i];
		}

		//take output array
		int output[size];
		//for stable array
		for(int i=size-1;i>=0;i--){
			output[count[arr[i]]-1]=arr[i];
			count[arr[i]]--;
		}
		for(int i=0;i<size;i++){
			arr[i]=output[i];
		}

}

void main(){

	int size;
	printf("Enter Size of array:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array Elements:\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	Counting(arr,size);

	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}


