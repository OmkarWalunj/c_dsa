//Quick sort

//mergesort in given array;

#include <stdio.h>
int partition(int*,int,int);
void Quick_sort(int * arr,int start,int end){

	if(start<end){

		int pivert=partition(arr,start,end);

		Quick_sort(arr,start,pivert-1);
		Quick_sort(arr,pivert+1,end);
	}
}
void swap(int * arr,int i,int itr){
	int temp=arr[i];
	arr[i]=arr[itr];
	arr[itr]=temp;
}

int partition(int arr[],int start,int end){
	int pivert=arr[end];
	int itr=start-1;
	for(int i=start;i<end;i++){
		if(arr[i]<pivert){
			itr++;
			swap(arr,i,itr);
		}
	}
	swap(arr,end,itr+1);
	return itr+1;

}

void main(){

	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array Elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	Quick_sort(arr,0,size-1);
	printf("Sorted Array :\n");
	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
