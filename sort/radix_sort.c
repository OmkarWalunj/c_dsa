// radix Sort

#include <stdio.h>
void Counting(int *,int,int);
void radix(int *arr,int size){

	int max=arr[0];
	for(int i=1;i<size;i++){

		if(max < arr[i]){
			max=arr[i];
		}
	}

	for(int pos=1;max/pos>0;pos=pos*10){
		Counting(arr,size,pos);
	}
}

void Counting(int * arr,int size,int pos){
		int count[10];
		for(int i=0;i<10;i++){
			count[i]=0;
		}
		for(int i=0;i<size;i++){
			count[(arr[i]/pos)%10]++;
			
		}
		printf("Prefixcount:\n");
		//prefixcount
		for(int i=1;i<10;i++){
			count[i]=count[i-1]+count[i];
			printf("%d ",count[i]);
		}
		printf("\n");

		//take output array
		int output[size];
		//for stable array
		for(int i=size-1;i>=0;i--){
			output[count[(arr[i]/pos)%10]-1]=arr[i];
			count[(arr[i]/pos)%10]--;
		}
		for(int i=0;i<size;i++){
			arr[i]=output[i];
		}
		printf("Output: ");
		for(int i=0;i<size;i++){
			printf("%d ",arr[i]);
		}
		printf("\n");

}

void main(){

	int size;
	printf("Enter Size of array:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array Elements:\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	radix(arr,size);

	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}


