//Selection_sort

#include <stdio.h>

void Selection_sort(int * arr,int size){

	for(int i=0;i<size;i++){
		int minindex=i;
		for(int j=i;j<size-1;j++){
			if(arr[minindex]>arr[j+1]){
				minindex=j;
			}
		}
		int temp=arr[i];
		arr[i]=arr[minindex];
		arr[minindex]=temp;
	}
}
void main(){

	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];
	printf("ENter ARRaY Elements;\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	Selection_sort(arr,size);
	printf("Sorted arr :\n");

	for(int i=0;i<size;i++){
		printf("%d ",arr[i]);
	}
	printf("\n");
}
