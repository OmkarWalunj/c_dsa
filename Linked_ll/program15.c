#include <stdio.h>
#include <stdlib.h>

typedef struct Bank{

        char bname[20];
        int account;
        float invest;
        struct Bank * next ;
}Bank;

Bank * head = NULL;

Bank * createnode(){
        Bank * newnode = (Bank *) malloc ( sizeof(Bank));
	getchar();
        printf("Enter Bank Name : \n");
        char ch;
        int i=0;
        while( (ch=getchar()) != '\n'){

                newnode -> bname[i]=ch;
                i++;
        }

        printf("Enter accounts : \n");
        scanf("%d",&newnode -> account);

        printf("Enter Investment:\n");
        scanf("%f",& newnode-> invest);

        newnode -> next =NULL;

        return newnode;
}

void addnode(){

	Bank * newnode=createnode();

	if(head == NULL){
		head = newnode;
	}else {
		Bank * temp = head;

		while(temp ->next != NULL){
			temp =temp ->next;
		}
		temp ->next =newnode;
	}
}
int count(){
	Bank * temp =head;
	
	int count =0;
	while( temp != NULL){
		count++;
		temp = temp -> next;
	}
	return count;
}

void addFirst(){
	Bank * newnode =createnode();

	if(head ==NULL){
		head = newnode;
	}else{
		newnode ->next = head;
		head =newnode;
	}
}

int addAtPos(){
	int pos;
	printf("\nEnter Position of node you have to add :\n");
	scanf("%d",&pos);
	
	int Count = count();
	if(pos <=0  || pos >= Count+2){
		printf("Invalid position \n");
		return -1;
	}else {
		if(pos == 1){
			addFirst();
		}else if(pos == Count+1){
			addnode();
		}else{

			Bank * newnode =createnode();
			Bank * temp = head;
			while(pos - 2){
				temp = temp->next;
				pos--;
			}
			newnode ->next = temp ->next;
			temp->next = newnode;
		}
		return 0;
	}
}

void addLast(){
	addnode();
}

void printLL(){
	Bank * temp = head;
	int Count = count();
	if(Count >= 1){
		while(temp->next !=NULL){
			printf("|%s ->",temp-> bname);
			printf("%d->",temp-> account);
			printf("%f|",temp-> invest);
			temp = temp-> next;
		}
		printf("|%s ->",temp-> bname);
                printf("%d->",temp-> account);
                printf("%f|",temp-> invest);

	}else {
		printf("Linked List is Empty\n");
	}
	printf("\n");
}

void deleteFirst(){

	Bank * temp = head;
	head= temp -> next;
	free(temp);
}

void deleteLast(){
	int Count = count();
	Bank * temp = head;
	if(Count == 1){
		head = NULL;
	}else if (Count > 1){
		while(temp -> next-> next != NULL){
			temp=temp -> next;
		}
		free(temp -> next);
		temp -> next =NULL;
	}else{
		printf("Linked is Empty\n");
	}
}
int deleteAtPos(){
        int pos;
        printf("\nEnter Position of node you have delete :\n");
        scanf("%d",&pos);

        int Count = count();
        if(pos <= 0 || pos > Count){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos == Count ){
			deleteLast();
		}else if(pos == 1){
			deleteFirst();
		}else{

                	Bank * temp = head;
			Bank * temp1 ;
                	while(pos - 2){
                        	temp = temp->next;
                        	pos--;
                	}
                	temp1 = temp ->next -> next;
			free(temp ->next);
			temp ->next = temp1;
		}
		return 0;
	}
}

void main(){

	char choice;
	do{
		printf(" ****************** Main *************\n");
		printf(" 1.addnode\n 2.addFirst\n 3.addAtPos\n 4.addAtLast\n 5.count\n 6.printLL\n 7.deleteFirst\n 8.deleteLast\n 9.deleteAtPos\n");

		int ch;
		printf("Enter Your choice : \n");
		scanf("%d",&ch);

		switch (ch){
                	case 1:
                        	addnode();
                        	break;
                	case 2:
                        	addFirst();
                        	break;
                	case 3:
                        	addAtPos();
                        	break;
                	case 4:
                        	addLast();
                        	break;
                	case 5:
				{
                        		int Count=count();
					printf("Count : %d\n",Count);
				}
                       		break;
                	case 6:
                        	printLL();
                        	break;
			case 7:
				deleteFirst();
				break;
			case 8:
				deleteLast();
				break;
			case 9:
				deleteAtPos();
				break;
                	default :
                        	printf("Wrong Input\n");
        	}
		
		getchar();
		printf("Do you have continue:\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}

     


	

