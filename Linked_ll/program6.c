// singly linked list using function  // 3rd approch 

#include <stdio.h>
#include <stdlib.h>

typedef struct student{


	int id;
	float ht;
	struct student * next ;
}stud;

stud * head = NULL;

void addnode(){
	stud * newnode = (stud *) malloc ( sizeof(stud));

	newnode -> id = 1;
	newnode -> ht = 5.6;
	newnode -> next =NULL;

	head = newnode;
};

void print(){
	stud * temp =head;

	while( temp != NULL){

		printf("%d = ",temp -> id);
		printf("%f |",temp -> ht);
		temp = temp -> next;
	}
};

void main(){
	addnode();
	print();
	printf("\n");
}

