#include <stdio.h>
#include <stdlib.h>

typedef struct node{

	int data ;
	int priority;
	struct node * next;
}node;

node *head =NULL;

node * createnode(){
	node * newnode = (node *)malloc(sizeof(node));
	
	printf("Enter Data :\n");
	scanf("%d",&newnode -> data);
	while(1){
		printf("Enter Priority :\n");
		scanf("%d",&newnode ->priority);

		if(newnode -> priority <= 5 && newnode -> priority >=0){
			break;
		}else {
			printf("Wrong priority\n");
		}
	}
	newnode -> next =NULL;

	return newnode;
}


int sortbyadd(){
	node * newnode = createnode();

	if(head== NULL){
		head= newnode;
	}else{
		node * temp = head;
		if(head->next == NULL){
			if(newnode -> priority > head-> priority){

				newnode->next = head;
				head= newnode;
				return 0;
			}else{

				head->next = newnode;
				return 0;
			}
		}else{
			if( newnode -> priority > head->priority){

                                newnode->next =head;
				head=newnode;
                                return 0;
                        }

			while(temp->next != NULL){
				if( newnode -> priority == temp -> priority){
					temp=temp->next;
				}else {
					if( temp-> priority >= newnode -> priority && newnode -> priority >= temp->next -> priority){
						newnode -> next = temp-> next;
						temp -> next = newnode;
        	                        return 0;
					}	
				}
				temp=temp->next;
			}
			temp->next =newnode;
			return 0;
		}
	}
}



void printLL(){
	node * temp = head;
	if(head== NULL){
		printf("Linked List is Empty\n");
	}else{
		while(temp !=NULL){
			printf("|%d|",temp-> data);
			temp = temp-> next;
		}
	}
	printf("\n");
}

int deleteFirst(){
	if(head == NULL){
		printf("Linked List is Empty \n");
		return -1;
	}else{

		node * temp = head;
		head= temp -> next;
		free(temp);
		return 0;
	}
}

void main(){

	char choice;
	do{
		printf(" ****************** Main *************\n");
		printf(" 1.sortbyadding\n 2.printLL\n 3.deleteFirst\n");

		int ch;
		printf("Enter Your choice : \n");
		scanf("%d",&ch);

		switch (ch){
                	case 1:
                        	sortbyadd();
                        	break;
                	case 2:
                        	printLL();
                        	break;
			case 3:
				deleteFirst();
				break;
                	default :
                        	printf("Wrong Input\n");
        	}
		
		getchar();
		printf("Do you have continue:\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}

     


	

