/* Singly circlular Linked List*/

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node*head=NULL;

node * createnode(){
	node *newnode=(node*)malloc(sizeof(node));
	getchar();
	printf("Enter Data:\n");
	scanf("%d",&newnode->data);

	newnode->next=NULL;

	return newnode;
}

void addnode(){
	node *newnode=createnode();

	if(head==NULL){
		head=newnode;
		newnode->next=head;
	}else{
		node *temp=head;
		while(temp->next !=head){
			temp=temp->next;
		}
		temp->next=newnode;
		newnode->next=head;
	}
}

void addFirst(){
	node *newnode=createnode();

	if(head==NULL){
		head=newnode;
		newnode->next=head;
	}else{
		node *temp=head;
		while(temp->next != head){
			temp=temp->next;
		}
		newnode->next=head;
		head=newnode;
		temp->next=head;
	}
}
int countnode(){
	node *temp=head;
	int count=0;
	while(temp->next !=head){
		count++;
		temp=temp->next;
	}
	return count+1;
}

int addAtpos(){
	int pos;
	printf("Enter Position :\n");
	scanf("%d",&pos);

	int count=countnode();

	if(pos<=0 || pos >count+1){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==count+1){
			addnode();
		}else{
			node *newNode = createnode();
			node *temp = head;

			while(pos-2){
			
				temp = temp->next;
				pos--;
			}

			newNode->next = temp->next;
			temp->next = newNode;
		}
	}
}

int deleteFirst(){
	int count=countnode();
	if(head==NULL){
		printf("Linked List is Empty\n");
		return -1;
	}else{
		if(count==1){
			free(head);
			head=NULL;
		}else{
			node *temp=head;
                	while(temp->next != head){
                        	temp=temp->next;
               		}
			node * temp2=head;
			head=head ->next;
			temp->next=head;
			free(temp2);
		}
		return 0;
	}
}
int deleteLast(){
	int count=countnode();
	if(head==NULL){
		printf("Linked List Is Empty\n");
		return -1;
	}else{
		if(count==1){
			deleteFirst();
		}else{
			node *temp =head;
			while(temp->next->next != head){
				temp=temp->next;
			}
			free(temp->next);
			temp->next=head;
		}
		return 0;
	}
}
int deleteAtpos(){
	int pos;
	printf("Enter Position:\n");
	scanf("%d",&pos);

	int count=countnode();
	if(pos<=0 || pos>count){
		printf("Invalid position\n");
		return -1;
	}else{
		if(count ==1){
			deleteFirst();
		}else if(count==pos){
			deleteLast();
		}else{
			node * temp=head;
			node * temp1=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			temp1=temp->next->next;
			free(temp->next);
			temp->next=temp1;
		}
		return 0;
	}
}

int printLL(){
	if(head==NULL){
		printf("Linked is Empty\n");
		return -1;
	}else{
		node *temp=head;
		while(temp->next!=head){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
		return 0;
	}
}

void main(){

	char choice;
	do{
		printf("*******Singly Circular opration *****\n");
		printf("1.Insert\n 2.addFirst\n 3.addLast\n 4.addatpos\n 5.deleteFirst\n 6.deleteAtpos\n 7.deleteLast\n 8.printLL\n \n");
		int ch;
		printf("Enter your choice:\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				addnode();
				break;
			case 2:
                                addFirst();
                                break;
			case 3:
                                addnode();
                                break;
			case 4:
                                addAtpos();
                                break;
			case 5:
                                deleteFirst();
                                break;
			case 6:
                                deleteAtpos();
                                break;
			case 7:
                                deleteLast();
                                break;
			case 8:
                                printLL();
                                break;
			default:
				printf("Wrong choice\n");
				break;
		}
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&choice);
	}while(choice=='Y' || choice=='y');
}
