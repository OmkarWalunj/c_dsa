#include <stdio.h>
#include <stdlib.h>

typedef struct Demo{

	int data ;
	struct Demo * next;
}Demo;

Demo *head =NULL;

Demo * createnode(){
	Demo * newnode = (Demo *)malloc(sizeof(Demo));
	
	printf("Enter Data :\n");
	scanf("%d",&newnode -> data);

	newnode -> next =NULL;

	return newnode;
}

void addnode(){

	Demo * newnode=createnode();

	if(head == NULL){
		head = newnode;
	}else {
		Demo * temp = head;

		while(temp ->next != NULL){
			temp =temp ->next;
		}
		temp ->next =newnode;
	}
}
int count(){
	Demo * temp =head;
	
	int count =0;
	while( temp != NULL){
		count++;
		temp = temp -> next;
	}
	return count;
}

void addFirst(){
	Demo * newnode =createnode();

	if(head ==NULL){
		head = newnode;
	}else{
		newnode ->next = head;
		head =newnode;
	}
}

void addAtPos(){
	int pos;
	printf("\nEnter Position of node you have to add :\n");
	scanf("%d",&pos);
	
	int Count = count();
	if(pos <= Count && pos > 1){
		Demo * newnode =createnode();
		Demo * temp = head;
		while(pos - 2){
			temp = temp->next;
			pos--;
		}
		newnode ->next = temp ->next;
		temp->next = newnode;
	}else if (pos == 1){
		addFirst();
	}else{
		printf("Enter position between 1 to %d\n ",Count);
	}
}

void addLast(){
	addnode();
}

void printLL(){
	Demo * temp = head;
	int Count = count();
	if(Count >= 1){
		while(temp !=NULL){
			printf("|%d|",temp-> data);
			temp = temp-> next;
		}
	}else {
		printf("Linked List is Empty\n");
	}
	printf("\n");
}

void deleteFirst(){

	Demo * temp = head;
	head= temp -> next;
	free(temp);
}

void deleteLast(){
	int Count = count();
	Demo * temp = head;
	if(Count == 1){
		head = NULL;
	}else if (Count > 1){
		while(temp -> next-> next != NULL){
			temp=temp -> next;
		}
		free(temp -> next);
		temp -> next =NULL;
	}else{
		printf("Linked is Empty\n");
	}
}
void deleteAtPos(){
        int pos;
        printf("\nEnter Position of node you have delete :\n");
        scanf("%d",&pos);

        int Count = count();
        if(pos <= Count && pos > 1){
                Demo * temp = head;
		Demo * temp1 ;
                while(pos - 2){
                        temp = temp->next;
                        pos--;
                }
                temp1 = temp ->next -> next;
		free(temp ->next);
		temp ->next = temp1;
        }else if (pos == 1){
                deleteFirst();
        }else{
                printf("Enter position between 1 to %d\n ",Count);
        }
}

void main(){

	char choice;
	do{
		printf(" ****************** Main *************\n");
		printf(" 1.addnode\n 2.addFirst\n 3.addAtPos\n 4.addAtLast\n 5.count\n 6.printLL\n 7.deleteFirst\n 8.deleteLast\n 9.deleteAtPos\n");

		int ch;
		printf("Enter Your choice : \n");
		scanf("%d",&ch);

		switch (ch){
                	case 1:
                        	addnode();
                        	break;
                	case 2:
                        	addFirst();
                        	break;
                	case 3:
                        	addAtPos();
                        	break;
                	case 4:
                        	addLast();
                        	break;
                	case 5:
				{
                        		int Count=count();
					printf("Count : %d\n",Count);
				}
                       		break;
                	case 6:
                        	printLL();
                        	break;
			case 7:
				deleteFirst();
				break;
			case 8:
				deleteLast();
				break;
			case 9:
				deleteAtPos();
				break;
                	default :
                        	printf("Wrong Input\n");
        	}
		
		getchar();
		printf("Do you have continue:\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}

     


	

