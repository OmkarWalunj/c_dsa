// singly linked list using function  

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Movie{


	int count;
	char mname[20];
	float price;
	struct Movie * next ;
}MV;

MV * head = NULL;

void addnode(){
	MV * newnode = (MV *) malloc ( sizeof(MV));

	newnode -> count = 1;
	strcpy(newnode -> mname,"Kantara");
	newnode -> price = 250.06;
	newnode -> next =NULL;

	head = newnode;
};

void print(){
	MV * temp =head;

	while( temp != NULL){

		printf("%d = ",temp -> count);
		printf("%s =",temp -> mname);
		printf("%f |",temp -> price);
		temp = temp -> next;
	}
};

void main(){
	addnode();
	print();
	printf("\n");
}

