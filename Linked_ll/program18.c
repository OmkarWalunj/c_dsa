/* reverse doubly  linked list second approach**/

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	struct node *prev;
	int data;
	struct node *next;
}node;

node *head=NULL;
node *temp=NULL;
node *createnode(){
	node * newnode=(node *)malloc(sizeof(node));
	newnode->prev=NULL;
	printf("Enter Data:\n");
	scanf("%d",&newnode->data);
	temp=newnode;
	newnode->next=NULL;

	return newnode;
}

void addnode(){
	node *newnode =createnode();

	if(head==NULL){
		head=newnode;
	}else{
		node *temp=head;
		while(temp->next !=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
		newnode->prev=temp;
	}
}
int countnode(){
	node *temp =head;
	int count=0;
	while(temp !=NULL){
		count++;
		temp=temp->next;
	}
	return count;
}
int rev(){
	int count=countnode();
	node * temp1=head;
	node * temp2=temp;
	int cnt=count/2;
	if(head==NULL){
		printf("Linked list is Empty\n");
		return -1;
	}else{
		if(count==1){
			return 0;
		}else{
			while(cnt){
				int num=temp2->data;
				temp2->data=temp1->data;
				temp1->data=num;
				temp1=temp1->next;
				temp2=temp2->prev;
				cnt--;
			}
		}
		return 0;
	}
	
}
void printLL(){
	node * temp=head;

	if(head==NULL){
		printf("Linked list is Empty \n");
	}else{
		while(temp->next!=NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void main(){
	int countnode;
	printf("Enter no of node:\n");
	scanf("%d",&countnode);

	for(int i=0;i<countnode;i++){
		addnode();
	}
	rev();
	printLL();
}
