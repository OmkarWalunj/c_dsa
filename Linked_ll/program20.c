//Doubly circular Linked List
#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	struct node *prev;
	int data;
	struct node *next;
}node;

node *head=NULL;

node *createnode(){
	node *newnode=(node *)malloc(sizeof(node));

	newnode->prev=NULL;

	printf("Enter data:\n");
	scanf("%d",&newnode->data);

	newnode->next=NULL;

	return newnode;
}

void addnode(){
	node * newnode=createnode();

	if(head==NULL){
		head=newnode;
		newnode->next=head;
		newnode->prev=head;
	}else{
		head->prev->next=newnode;
		newnode->next=head;
		newnode->prev=head->prev;
		head->prev=newnode;
	}
}
void addFirst(){
	node *newnode=createnode();
	if(head==NULL){
		head=newnode;
		newnode->next=head;
		newnode->prev=head;
	}else{
		newnode->next=head;
		newnode->prev=head->prev;
		head=newnode;
		newnode->next->prev=newnode;
		newnode->prev->next=head;
	}
}
int countnode(){
	node *temp=head;
	int count=0;
	if(head==NULL){
		return count;
	}else{
		while(temp->next != head){
			count++;
			temp=temp->next;
		}
		return count+1;
	}
}
int addAtpos(int pos){
	int count=countnode();
	if(pos<=0 || pos>count+1){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos==1){
			addFirst();
		}else if(pos==count+1){
			addnode();
		}else{

			node *newnode=createnode();
			node *temp=head;
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newnode->next=temp->next;
			newnode->prev=temp;
			temp->next=newnode;
			newnode->next->prev=newnode;
		}
		return 0;
	}
}
int deleteFirst(){
	if(head==NULL){
		printf("Linked list is Empty\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head=NULL;
		}else{
			head->next->prev=head->prev;
			head=head->next;
			free(head->prev->next);
			head->prev->next=head;
		}
		return 0;
	}
}

int deleteLast(){
	if(head==NULL){
		printf("Linked list is Empty\n");
		return -1;
	}else{
		if(head->next == head){
			free(head);
			head=NULL;
		}else{
			struct node * temp=head->prev->prev;
			free(temp->next);
			temp->next=head;
			head->prev=temp;
		}
		return 0;
	}
}
int deleteAtpos(int pos){
	int count=countnode();

	if(pos<=0 || pos>count){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos==1){
			deleteFirst();
		}else if(pos==count){
			deleteLast();
		}else{
			node *temp1=head;
			node *temp2=head;

			while(pos-2){
				temp1=temp1->next;
				pos--;
			}
			temp2=temp1->next->next;
			free(temp1->next);
			temp1->next=temp2;
			temp2->prev=temp1;
		}
		return 0;
	}
}

void printLL(){
	node *temp=head;
	if(head==NULL){
		printf("Linked List is Empty\n");
	}else{
		while(temp->next != head){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);
	}
}

void main(){
	char choice;
	do{
		printf("*************** main *****************\n");
		printf(" 1.addnode\n 2.addFirst\n 3.addLast\n 4.addAtpos\n 5.deleteFirst\n 6.deleteLast\n 7.deleteAtpos\n 8.PrintLL\n");
		int ch;
		printf("Enter choice:\n");
		scanf("%d",&ch);

		switch (ch){
			case 1:
				addnode();
				printLL();
				break;
			case 2:
				addFirst();
				printLL();
				break;
			case 3:
				addnode();
				printLL();
				break;
			case 4:
				{
					int pos;
					printf("Enter position :\n");
					scanf("%d",&pos);
					addAtpos(pos);
					printLL();
				}
				break;
			case 5:
				deleteFirst();
				printLL();
				break;
			case 6:
				deleteLast();
				printLL();
				break;
			case 7:
				 {
                                       int pos;
                                       printf("Enter position :\n");
                                       scanf("%d",&pos);
                                       deleteAtpos(pos);
                                       printLL();
				}
				break;
			case 8:
				printLL();
				break;
			default:
				printf("Wrong input\n");
				break;
		}

		getchar();
		printf("Do U want Continue :\n");
		scanf("%c",&choice);
	}while((choice=='Y') || (choice=='y'));
}


