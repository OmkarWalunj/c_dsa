// singly linked list using function  get values from user

#include <stdio.h>
#include <stdlib.h>

typedef struct Movie{

	char mname[20];
	float IMDB;
	struct Movie * next ;
}MV;

MV * head = NULL;

void addnode(){
	MV * newnode = (MV *) malloc ( sizeof(MV));
	
	printf("Enter movie name :\n");
	fgets(newnode -> mname,15,stdin);
	
	printf("Enter IMDB rating:\n");
	scanf("%f",&(newnode -> IMDB));
	
	getchar();
	newnode -> next =NULL;
	
	if(head == NULL){
		head=newnode;
	}else{
		MV * temp =head;
		while(temp -> next != NULL){
			temp =temp-> next;
		}
		temp->next = newnode;
	}
};

void print(){
	MV * temp =head;

	while( temp != NULL){
		printf("%s =",temp -> mname);
		printf("%f |",temp -> IMDB);
		temp = temp -> next;
	}
};

void main(){
	addnode();
	addnode();
	addnode();
	print();
	printf("\n");
}

