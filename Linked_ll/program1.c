//Singly Linked List

#include <stdio.h>
#include <stdlib.h>

struct Node{
	int data;
	struct Node * next;
};

void main(){

	struct Node * head = NULL;
	//create Node
	struct Node * newNode = (struct Node*)malloc (sizeof(struct Node));
	
	//Insert data into node;
	newNode -> data = 10;
	newNode -> next = NULL;
	
	//connecting head to node
	
	head = newNode;

	//create second node
	newNode = (struct Node*)malloc (sizeof(struct Node));

	//Insert data into node;
        newNode -> data = 20;
        newNode -> next = NULL;

	//connecting second node to 1st node
	head -> next = newNode;

	//create third node	
	newNode = (struct Node*)malloc (sizeof(struct Node));

	//Insert data into node;
        newNode -> data = 30;
        newNode -> next = NULL;

	//connecting third node to 2nd node
        head -> next ->next = newNode;

	struct Node * temp = head;
	// print data
	while( temp != NULL){
		printf("%d ",temp -> data);
		temp = temp->next;
	}
	printf("\n");
}

	


