// singly linked list using function  // 2 nd approch ;

#include <stdio.h>
#include <stdlib.h>

typedef struct student{


	int id;
	float ht;
	struct student * next ;
}stud;

void addnode(stud ** head){
	stud * newnode = (stud *) malloc ( sizeof(stud));

	newnode -> id = 1;
	newnode -> ht = 5.6;
	newnode -> next =NULL;

	*head = newnode;
}

void print( stud * head){
	stud * temp =head;

	while( temp != NULL){

		printf("%d = ",temp -> id);
		printf("%f |",temp -> ht);
		temp = temp -> next;
	}
}
void main(){

	stud * head = NULL;

	addnode(&(head));
	print((head));
	printf("\n");
}

