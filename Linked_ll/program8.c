// singly linked list using function  get values from user

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Movie{

	char mname[20];
	float IDMI;
	struct Movie * next ;
}MV;

MV * head = NULL;

void addnode(){
	MV * newnode = (MV *) malloc ( sizeof(MV));
	
	printf("Enter movie name :\n");
	fgets(newnode -> mname,15,stdin);
	
	printf("Enter IDMI rating:\n");
	scanf("%f",&(newnode -> IDMI));
	
	getchar();
	newnode -> next =NULL;
	head = newnode;
};

void print(){
	MV * temp =head;

	while( temp != NULL){
		
		printf("%s =",temp -> mname);
		printf("%f |",temp -> IDMI);
		temp = temp -> next;
	}
};

void main(){
	addnode();
	print();
	printf("\n");
}

