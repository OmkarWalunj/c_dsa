#include <stdio.h>
#include <stdlib.h>

typedef struct Demo {

	int data;
	struct Demo * next;
}Demo;

Demo * head = NULL;

Demo * createnode(){
	Demo * newnode = (Demo *)malloc(sizeof(Demo));
	getchar();
	printf("Enter Data:\n");
	scanf("%d",&(newnode -> data));

	newnode -> next =NULL;
}

void addnode(){

	Demo * newnode = createnode();
	if(head == NULL){
		head = newnode;
	}else{
		Demo * temp =head;
		while(temp ->next != NULL){
			temp = temp->next;
		}
		temp ->next = newnode;
	}
}

void search(int node){
	int input;
	printf("Enter Number:\n");
	scanf("%d",&input);

	int count=0,count1=0,pos=0,prev=0;
	Demo * temp = head;
	while( temp != NULL){
		count++;
		if(input == temp -> data){
			count1++;
			prev = pos;
			pos = count;
			
		}

		temp= temp -> next;
	}
	if(count1 ==0){
		printf("Wrong input\n");
	}else if( count1==1){
		printf("Number is present only once\n");
	}else{
		printf("Position of last Occurance:%d\n",prev);
	}
}

void main(){
	int node;
	printf("Enter no of node :\n");
	scanf("%d",&node);

	for (int i=0;i<node;i++){
		addnode();
	}

	search(node);
}
