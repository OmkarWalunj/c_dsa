/* Doubly Linked List */
#include <stdio.h>
#include<stdlib.h>

typedef struct Node{
	
	struct Node * prev;
	int data;
	struct Node * next;
}Node;

Node * head = NULL;

Node * createnode(){
	Node * newnode = (Node *)malloc ( sizeof(Node));

	newnode -> prev = NULL;

	printf("Enter Data :\n");
	scanf("%d",&newnode -> data);

	newnode -> next =NULL;

	return newnode;
}

void addnode(){
	
	Node * newnode = createnode();

	if(head ==NULL){
		head = newnode;
	}else{
		Node * temp =head;
		while( temp -> next != NULL){
			temp =  temp -> next;
		}
		temp -> next = newnode;
		newnode -> prev = temp ;
	}

}

void addFirst(){
	Node * newnode = createnode();

	if(head ==NULL){
		head=newnode;
	}else{
		newnode -> next = head;
		head -> prev = newnode;
		head = newnode;
	}
}
int countnode(){
	Node * temp = head ;
	int count =0;
	while ( temp != NULL){
		count++;
		temp = temp -> next ;
	}
	return count;
}
int  addAtPos(){
	int pos;
	printf("Enter Position:\n");
	scanf("%d",&pos);

	int count = countnode();

	if(pos <=0 || pos >= count +2){
		printf("Invalid Position\n");
		return -1;
	}else{
		if(pos == 1){
			addFirst();
		}else if( pos == count +1){
			addnode();
		}else{
			Node * temp = head;
			Node  * newnode = createnode();

			while(pos -2){
				temp = temp->next;
				pos--;
			}
			newnode -> next =  temp -> next;
			newnode -> prev = temp;
			temp -> next -> prev = newnode;
			temp -> next = newnode;
		}
		return 0;
	}
}
void printDLL(){
	Node * temp = head;
	int count = countnode();
	if(count >= 1){
		while(temp -> next != NULL){
			printf("|%d|->",temp -> data);
			temp =temp -> next;
		}

		printf("|%d|\n",temp -> data);
	}else{
		printf("Linked List is Empty\n");
	}
}
int deleteFirst(){
	int count = countnode();
	if( head ==NULL){
		printf("Linked list is already empty\n");
		return -1;
	}else{
		if(count == 1){
			free(head);
			head= NULL;
		}else{
			head= head -> next;
			free(head -> prev);
			head -> prev = NULL;
		}
		return 0;
	}
}
int deleteLast(){
	int count =  countnode();
	if( head == NULL){
		printf("Linked list is already Empty\n");
		return -1;
	}else{
		if(count ==1){
			free(head);
			head = NULL;
		}else{
			Node * temp =head;
			while(temp -> next -> next != NULL){
				temp = temp ->next;
			}
			free (temp -> next);
			temp -> next = NULL;
		}
		return 0;
	}
}
int deleteAtPos(){
	int pos;
        printf("Enter Position:\n");
        scanf("%d",&pos);

        int count = countnode();

        if(pos <=0 || pos > count ){
                printf("Invalid Position\n");
                return -1;
        }else{
                if(pos == 1){
                        deleteFirst();
                }else if( pos == count ){
                        deleteLast();
                }else{
                        Node * temp = head;
			Node * temp1 ;
                        while(pos -2){
                                temp = temp->next;
                                pos--;
                        }
                        temp1= temp -> next -> next;
			free(temp -> next);
			temp1 -> prev = temp;
			temp -> next = temp1;
                }
                return 0;
        }
}
void main(){

	char choice;

	do{
		printf(" ****************** Main *************\n");
		printf(" 1.addnode\n 2.addFirst\n 3.addAtPos\n 4.addAtLast\n 5.count\n 6.printLL\n 7.deleteFirst\n 8.deleteLast\n 9.deleteAtPos\n");

		int ch;
		printf("Enter Your choice : \n");
		scanf("%d",&ch);

		switch (ch){
                	case 1:
                        	addnode();
                        	break;
                	case 2:
                        	addFirst();
                        	break;
                	case 3:
                        	addAtPos();
                        	break;
                	case 4:
                        	addnode();
                        	break;
                	case 5:
				{
                        		int Count=countnode();
					printf("Count : %d\n",Count);
				}
                       		break;
                	case 6:
                        	printDLL();
                        	break;
			case 7:
				deleteFirst();
				break;
			case 8:
				deleteLast();
				break;
			case 9:
				deleteAtPos();
				break;
                	default :
                        	printf("Wrong Input\n");
        	}

		getchar();
		printf("Do you have continue:\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}

