/* WAP to used singly linked list functions 
 * 	1.createnode
 * 	2.printll
 * 	3.count
 * 	4.addfirst
 * 	5.addposition
 * 	6.addlast
 *
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct Employee{
	char name[20];
	int id;
	struct Employee * next;
}Emp;

Emp *head = NULL;

Emp * createnode(){
	Emp * newnode =(Emp *)malloc(sizeof(Emp));

	getchar();

	printf("Enter Employee Name : \n");
	char ch;
	int i=0;
	while( (ch=getchar()) != '\n'){

		newnode -> name[i]=ch;
		i++;
	}

	printf("Enter Employee id : \n");
	scanf("%d",&newnode -> id);

	newnode -> next =NULL;
	
	return newnode;
}

void addnode(){

	Emp * newnode =createnode();

	if(head == NULL){
		head = newnode;
	}else{
		Emp * temp =head;

		while ( temp -> next != NULL){

			temp =temp -> next ;
		}
		temp -> next = newnode;
	}
}

int count(){
	Emp * temp =head;
	
	int count =0;
	while( temp != NULL){
		count++;
		temp = temp -> next;
	}
	return count;
}

void addfirst(){

	Emp * newnode = createnode();

	if(head == NULL){
		head = newnode;
	}else{
		newnode-> next =head ;
		head = newnode;
	}
}

void addPosition(){
	int pos;
	printf("Enter no of node you have to add:\n");
	scanf("%d",&pos);

	Emp * newnode = createnode();
	Emp * temp =head;
	int count1 = count();

	if(head == NULL){
		head = newnode;
	}else if(count1 >=pos){
	
		int p1=pos;
		while(p1-2){
			temp =temp ->next;
			p1--;
		}
		newnode -> next= temp ->next;
		temp -> next = newnode;
	}else{
		printf("Enter no is Greater than no of node in list\n");
	}
}

void addlast(){
        Emp * newnode =createnode();

        if(head == NULL){
                head = newnode;
        }else{
                Emp * temp =head;

                while ( temp -> next != NULL){

                        temp =temp -> next ;
                }
                temp -> next = newnode;
        }
}

void printLL(){
	Emp * temp = head;

	while( temp != NULL){
		printf("|%s->",temp -> name);
		printf("%d |",temp -> id);
		temp = temp ->next ;
	}
}

void main(){

	int node ;
	printf("Enter no of node:");
	scanf("%d",&node);

	for ( int i=0;i<node;i++){
		addnode();
	}

	printLL();
	int Count = count();
	printf("\nCount = %d",Count);
	printf("\n");
	addfirst();
	printLL();
	printf("\n");
	addPosition();
	printLL();
	printf("\n");
	addlast();
	printLL();
	printf("\n");

}







