//singly ll    // note :- This is not a proper approach

#include <stdio.h>
#include  <stdlib.h>

typedef struct student{
	int id;
	float ht;
	struct student *next;
}stud;

void main(){

	stud * head = NULL;
	//create node
	stud * newnode = (stud*)malloc (sizeof(stud));
	//insert data
	newnode -> id =1;
	newnode-> ht =5.5;
	newnode -> next = NULL;
	//connet head to node
	head = newnode;
	//create second node
	newnode = (stud*)malloc (sizeof(stud));
	//insert data
	
	newnode -> id =2;
        newnode-> ht =6.5;
        newnode -> next = NULL;

	//connecting node to head
	head -> next = newnode;

	//create third node 
	newnode = (stud* )malloc (sizeof(stud));

	//insert data
	newnode -> id =3;
        newnode-> ht =4.5;
        newnode -> next = NULL;

	//connecting third node head
	head  -> next -> next =newnode;

	//print data
	
	stud * temp = head;

	while( temp != NULL){

		printf("%d = ",temp -> id);
		printf("%f |",temp -> ht);
		temp = temp ->next;
	}
	printf("\n");
}


