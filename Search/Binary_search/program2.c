// single elements in sorted array

#include<stdio.h>

int singleElement(int*arr,int start ,int end){
	int index=-1;
	int count=0;
	if(start==end+1){
		return index;
	}
	if(arr[start] != arr[start+1]){
		index=start;
		return start;
	}
	return singleElement(arr,start+2,end);
}

void main(){
	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);

	int arr[size];
	printf("ENter array elements:\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int start=0;
	int end=size-1;
	int flag1=0;
	int flag2=0;
	int val=-1;
	int mid=(start+end)/2;
	if((arr[mid] != arr[mid-1]) && (arr[mid] != arr[mid+1])){
		val= mid;
	}
	if(arr[mid] == arr[mid-1]){
		flag1=1;
		printf("1\n");
		end=mid;
		val=singleElement(arr,start,end);
	}
	if(arr[mid] ==arr[mid+1]){
		flag2=1;
		printf("2\n");
		start=mid;
		end=size-1;
		val=singleElement(arr,start,end);
	}
	if(val==-1){
		if(flag1==1){
			printf("3\n");
			start=mid+1;
        	        end=size-1;
	                val=singleElement(arr,start,end);
		}
		if(flag2==1){
			printf("4\n");
			start=0;
			end=mid;
			val=singleElement(arr,start,end);
		}
	}

	printf("single element index is %d\n",val);
}



