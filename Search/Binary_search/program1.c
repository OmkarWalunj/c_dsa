// search key value and return index  using binaryb search
// input : arr[]={8,9,10,4,5,6,7}
// o/p: 4

#include<stdio.h>

int rotatesearch(int arr[],int key,int size){
	int point=-1;
	int start=0;
	int end=size-1;
	int i=0;
	int flag=0;

	while(start<=end){

		if(arr[i] <= arr[i+1]){
			point=++i;
			if(arr[point]==key){
				return point;
			}
		}else{
			if(flag ==0){
				flag=1;
				if(arr[point]<key){
                                	end=point-1;
                        	}
                        	if(arr[size-1]>key){
                                	start =point+1;
				}
                        }

			point=(start+end)/2;

			if(arr[point]==key){
				return point;
			}
			if(arr[point]>key){
                                end=point-1;
                        }
                        if(arr[point]<key){
                                start =point+1;
                        }
		}
	}
	printf("after while\n");
	return -1;
}
		
void main(){

	int size;
	printf("Enter Array size:\n");
	scanf("%d",&size);

	int arr[size];
	printf("Enter array element:\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	int num;
	printf("Enter search key:\n");
	scanf("%d",&num);

	printf("Index is:%d\n",rotatesearch(arr,num,size));
}
