//Find first occurance index of an key in arr

#include<stdio.h>

int floorval(int*arr,int key,int size){
	int start=0;
	int end=size-1;
	int val=-1;
	int mid;

	while(start<=end){
		mid=(start+end)/2;
		if(arr[mid]==key){
			val=mid;
			if(mid !=0){
				if(arr[mid] != arr[mid-1]){
					return mid;
				}
				end=mid-1;
			}
		}

		if(arr[mid]<key){
			start=mid+1;
		}
		if(arr[mid]>key){
			val=mid;
			end=mid-1;
		}
	}
	return val;
}

void main(){
	int size;
	printf("Enter size of arr:\n");
	scanf("%d",&size);

	int arr[size];
	printf("Enter array elements:\n");

	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}

	int key;
	printf("ENter search key:\n");
	scanf("%d",&key);

	int val=floorval(arr,key,size);

	if(val==-1){
		printf("index not possible\n");
	}else{
		printf("index of first occ of value is %d\n",val);
	}
}


