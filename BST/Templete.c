
#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>

struct BSTNode{

	int data;
	struct BSTNode*left;
	struct BSTNode*right;
};
struct BSTNode* insertInBst(struct BSTNode* root,int val){
	if(root ==NULL){
		struct BSTNode * newnode=malloc(sizeof(struct BSTNode));
		newnode->data=val;
		newnode->left=NULL;
		newnode->right=NULL;
		root=newnode;
		return root;
	}
	if(root->data >val){
		root->left=insertInBst(root->left,val);
	}else{
		root->right=insertInBst(root->right,val);
	}
	return root;

}

void inOrder(struct BSTNode*root){
	if(root==NULL){
		return;
	}
	inOrder(root->left);
	printf("%d ",root->data);
	inOrder(root->right);
}

bool searchInBst(struct BSTNode*root ,int ele){
	if(root==NULL){
		return false;
	}
	if (root->data== ele)
		return true;
	if(root->data > ele)
		return searchInBst(root->left,ele);
	else
		return searchInBst(root->right,ele);
}

bool iteraSearch(struct BSTNode* root,int ele){

	struct BSTNode* temp=root;

	while(temp != NULL){
		if(temp->data == ele)
			return true;
		if(temp->data > ele)
			temp=temp->left;
		else
			temp=temp->right;
	}
	return false;
}	 
int minInBst(struct BSTNode * root){

	struct BSTNode *temp=root;

	while(temp->left != NULL){
		temp=temp->left;
	}
	return temp->data;
}
int maxInBst(struct BSTNode * root){

	struct BSTNode *temp=root;

	while(temp->right != NULL){
		temp=temp->right;
	}
	return temp->data;
}


struct BSTNode * deleteInBst(struct BSTNode * root,int key){

	if(root==NULL){
		return root;
	}
	if((root->left ==NULL) && (root->right==NULL)){
		if(root->data==key){
			struct BSTNode*temp=root;
			root=NULL;
			free(temp);
			return root;
		}
	}
	if(root->data == key){
		if(root->left !=NULL){
			root->data=maxInBst(root->left);
			root->left=deleteInBst(root->left,root->data);
		}else{
			root->data=minInBst(root->right);
			root->right=deleteInBst(root->right,root->data);
		}
	}
	if(root->data > key){
		root->left=deleteInBst(root->left,key);
	}else if(root->data <key){
		root->right=deleteInBst(root->right,key);
	}
	return root;
}


void main(){

	struct BSTNode*root=NULL;
	char ch;

	do{
		printf("1.printINBst\n2.insertInBst\n3.searchInBst\n4.maxInBst\n5.minInBst\n6.deleteInBst\n");
		int choice;
		printf("Enter Your Choice\n");
		scanf("%d",&choice);

		switch(choice){
			case 1:{
					inOrder(root);
					printf("\n");
			       }
			       break;
			case 2:
			       {
				       int val;
				       printf("Enter Value:\n");
				       scanf("%d",&val);
				       root=insertInBst(root,val);
			       }
			       break;
			case 3:{

	
					int ele;
					printf("Enter Search Element:\n");
					scanf("%d",&ele);
					bool find=searchInBst(root,ele);
					//bool find=iteraSearch(root,ele);

					if(find==true)
						printf("Found\n");
					else
						printf("Not Found\n");
			       }
			       break;
			case 4:{
				       int max=maxInBst(root);
				       printf("Max In Bst:%d\n",max);
			       }
			case 5:{
				       int min=minInBst(root);
				       printf("Min In Bst:%d\n",min);
			       }
			       break;
			case 6:
			       {
				       int key;
				       printf("Enter delete Key:\n");
				       scanf("%d",&key);

				       root=deleteInBst(root,key);
			       }
			       break;
		}

		getchar();
		printf("Wan't Continue?\n");
		scanf("%c",&ch);
	}while(ch=='y' || ch=='y');

}
