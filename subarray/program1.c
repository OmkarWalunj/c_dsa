// print all subarray approach 1

#include <stdio.h>

void main(){

	int size;

	printf("Enter Array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter Array Elements:\n");

	for(int i=0;i<size;i++){
		scanf("%d", &arr[i]);
	}

	for(int i=0;i<size;i++){

		for(int j=0;j<size;j++){

			for(int k=i;k<=j;k++){
				printf("%d ",arr[k]);
			}
			printf("\n");
		}
	}
}

