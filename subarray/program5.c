// print the sum of every single subarray with time complexity o(N2) & without using extra space complexity 

#include <stdio.h>

void main(){

	int size;

	printf("Enter Array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter Array Elements:\n");

	for(int i=0;i<size;i++){
		scanf("%d", &arr[i]);
	}

	for(int i=0;i<size;i++){
		int sum=0;
		for(int j=i;j<size;j++){
			sum=sum+arr[j];
			printf("%d\n",sum);
		}
	}
}

