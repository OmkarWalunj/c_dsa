// print the sum of every single subarray using prefix sum

#include <stdio.h>

void main(){
	
	int size;
	printf("Enter Size of array:\n");
	scanf("%d",&size);
	int arr[size];
	int psum[size];
	printf("ENter array Elements:\n");
	for(int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	psum[0]=arr[0];
	for(int i=1;i<size;i++){
		psum[i]=psum[i-1]+arr[i];
	}
	for(int i=0;i<size;i++){
		for(int j=i;j<size;j++){
			int sum=0;
			if(j==0){
				sum=psum[i];
			}else{
				sum=psum[j]-psum[i]+arr[i];
			}
			printf("%d\n",sum);
		}
	}
}
