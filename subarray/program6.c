// maximum subarray : Brute force approach

#include <stdio.h>

void main(){

	int size;

	printf("Enter Array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter Array Elements:\n");

	for(int i=0;i<size;i++){
		scanf("%d", &arr[i]);
	}
	int maxsum=arr[0];
	for(int i=0;i<size;i++){
		for(int j=i;j<size;j++){
			int sum=0;
			for(int k=i;k<=j;k++){
				sum=sum+arr[k];
			}
			if(maxsum < sum){
				maxsum=sum;
			}
		}
	}
	printf("Maxsum subarray:%d\n",maxsum);	
}

