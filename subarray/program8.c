// maximum subarray : kadane's algorithm

#include <stdio.h>

void main(){

	int size;

	printf("Enter Array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter Array Elements:\n");

	for(int i=0;i<size;i++){
		scanf("%d", &arr[i]);
	}
	int maxsum=arr[0];
	int sum=0;
	for(int i=0;i<size;i++){
		sum=sum+arr[i];
		if(sum<0){
			sum=0;
		}
		if(maxsum < sum)
			maxsum=sum;
	}
	printf("Maxsum subarray:%d\n",maxsum);	
}

