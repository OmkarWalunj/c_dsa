// counting subarray 
// Given an array A of N non negative numbers and a non negative number B. you need to find the number of subarray in A with a Sum less than B.
// Return an integer denoting the number of subarrays in A having sum less than B

#include <stdio.h>

void main(){

	int size;

	printf("Enter Array size:\n");
	scanf("%d",&size);

	int arr[size];

	printf("Enter Array Elements:\n");

	for(int i=0;i<size;i++){
		scanf("%d", &arr[i]);
	}
	int B;
	printf("ENter Non negative integer:\n");
	scanf("%d",&B);

	int minB=0;
	for(int i=0;i<size;i++){
		int sum=0;
		for(int j=i;j<size;j++){
			sum=sum+arr[j];
			if(sum<B){
				minB++;
			}
		}

	}
	printf("count:%d\n",minB);
}

