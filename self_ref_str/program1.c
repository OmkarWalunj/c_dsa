#include <stdio.h>
#include <string.h>
typedef struct employee{
	int empid;
	char empname[20];
	float sal;
	struct employee * next;
}emp;
void main(){

	emp obj1,obj2,obj3;
	emp * head=&obj1;

	obj1.empid=1;
	strcpy(obj1.empname,"Omkar");
	obj1.sal=30.0;
	obj1.next=&obj2;
	
	obj2.empid=2;
	strcpy(obj2.empname,"Ram");
	obj2.sal=40.0;
	obj2.next=&obj3;
	
	obj3.empid=3;
	strcpy(obj3.empname,"Om");
	obj3.sal=50.0;
	obj3.next=NULL;
	
	printf("Emp id =%d\n",head->empid);
	printf("Emp name=%s\n",head -> empname);
	printf("Emp sal=%f\n",head -> sal);
	
	printf("Emp id =%d\n",obj1.next -> empid);
	printf("Emp name=%s\n",obj1.next -> empname);
	printf("Emp sal=%f\n", obj1.next -> sal);

	printf("Emp id =%d\n",obj2.next -> empid);
        printf("Emp name=%s\n",obj2.next -> empname);
        printf("Emp sal=%f\n", obj2.next -> sal);
}



	
