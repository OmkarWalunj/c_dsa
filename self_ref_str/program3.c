//self referancing structure using malloc

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Empolyee{

	int empid;
	char empname[20];
	float sal;
	struct Empolyee * next;
}Emp;

void main(){

	Emp *emp1=(Emp*)malloc(sizeof(Emp));
	Emp *emp2=(Emp*)malloc(sizeof(Emp));
	Emp *emp3=(Emp*)malloc(sizeof(Emp));
	
	Emp * head =  emp1;

	head -> empid =1;
	strcpy(head -> empname,"Omkar");
	head -> sal=56.0;
	head -> next = emp2;

	head -> next -> empid =2;
        strcpy(head -> next-> empname,"Tejas");
        head -> next-> sal=65.0;
        head -> next -> next = emp3;

	head -> next-> next-> empid =3;
        strcpy(head -> next-> next-> empname,"Om");
        head -> next-> next-> sal=56.0;
        head -> next-> next-> next = NULL;
	
	printf("%d\n",emp1->empid);
	printf("%s\n",emp1->empname);
	printf("%f\n",emp1->sal);
	printf("%p\n",emp1->next);
	
	printf("%d\n",emp2->empid);
        printf("%s\n",emp2->empname);
        printf("%f\n",emp2->sal);
        printf("%p\n",emp2->next);
	
	printf("%d\n",emp3->empid);
        printf("%s\n",emp3->empname);
        printf("%f\n",emp3->sal);
        printf("%p\n",emp3->next);
}




