// self referancing structure get values from user

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Movie{

	int count ;
	char Mname[20];
	float price;
	struct Movie * next ;
}MV ;

void getdata(MV * ptr1,MV *ptr2){
	printf("Enter No of count: ");
	scanf("%d",&(ptr1 -> count));

	printf("Enter Movie Name: ");
	scanf("%s",(ptr1->Mname));

	printf("Enter price : ");
	scanf("%f",&(ptr1 -> price));

	ptr1 -> next = ptr2;
}

void accessdata(MV * ptr1){
	printf("%d\n",ptr1 ->count);
	printf("%s\n",ptr1 ->Mname);
	printf("%f\n",ptr1 ->price);
	printf("%p\n",ptr1 ->next);
}

void main(){
	
	MV * m1=(MV*)malloc(sizeof(MV));
	MV * m2=(MV*)malloc(sizeof(MV));
	MV * m3=(MV*)malloc(sizeof(MV));

	getdata(m1,m2);
	printf("\n");
	getdata(m2,m3);
	printf("\n");
	getdata(m3,NULL);
	printf("\n");

	accessdata(m1);
	printf("\n");
	accessdata(m2);
	printf("\n");
	accessdata(m3);
}
