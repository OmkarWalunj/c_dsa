// sum of 1st 10 number by using recursion

#include<stdio.h>

int fun(int x){
	static int sum=0;

	sum=sum+x;
	if(x!= 1){
		fun(--x);
	}
	return sum;
}

void main(){
	printf("sum is :%d\n",fun(10));
}

