// find no of zeros in given user value by using recursion

#include <stdio.h>
int countzero(int );

void main(){

	int num;
	printf("Enter Number:\n");
	scanf("%d",&num);

	if(num >0){
		int count=countzero(num);
		printf("No of zeroes in %d is %d\n",num,count);
	}else{
		printf("No of zeroes in %d is 1\n",num);

	}
}

int countzero(int num){
	static int count=0;
	if(num==0){
		return count;
	}
	if(num % 10 ==0){
		count=count+1;
	}
	return countzero(num /10);
}
