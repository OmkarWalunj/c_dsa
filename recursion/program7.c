//Factorial of given number using recursion
#include <stdio.h>

int fact(int N){
	if(N == 1){
		return 1;
	}
	return N*fact(N-1);
}

void main(){

	printf("Factorial IS %d\n",fact(5));
}
