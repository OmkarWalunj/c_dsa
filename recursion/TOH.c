#include <stdio.h>

void TOH(int N,char From,char via, char To){

	if(N>0){

		TOH(N-1,From,To,via);
		printf("%c->%c\n",From,To);
		TOH(N-1,via,From,To);
	}
}

void main(){

	int N=2;
	TOH(N,'a','b','c');
}
