// find no of steps in given user value if number is even then divide and number is odd then subtract by 1 by using recursion

#include <stdio.h>
int countstep(int );

void main(){

	int num;
	printf("Enter Number:\n");
	scanf("%d",&num);
	if(num>0){
		int count=countstep(num);
		printf("No of steps in %d is %d\n",num,count);
	}else{
		 printf("No of steps in %d is 1",num);

	}
}

int countstep(int num){
	static int count=0;
	if(num==0){
		return count;
	}
	if(num % 2 ==0){
		count=count+1;
		return countstep(num /2);
	}
	if(num % 2 !=0){
		count=count+1;
		return countstep(num-1);
	}
}
