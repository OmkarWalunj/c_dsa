// reverse a number given from user using recursion

#include<stdio.h>

int rev(int );

void main(){
	int num;
	printf("Enter Number :\n");
	scanf("%d",&num);

	printf("Reverse of %d is %d\n",num,rev(num));
}

int rev(int num){

	static int reverse=0;
	if(num ==0){
		return reverse;
	}
	reverse=reverse*10+(num %10);
	return rev(num/10);
}
