// search character in character array using recursion

#include <stdio.h>
#include<stdbool.h>

bool searchchar(char*arr,char ch,int N){

	if(N==0){
		return false;
	}
	if(ch== arr[N-1]){
		return true;
	}
	return searchchar(arr,ch,N-1);
}

void main(){

	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);
	char arr[size];
	printf("Enter string:\n");
	scanf("%s",arr);
	
	char ch;
	printf("ENter search character:\n");
	scanf(" %c",&ch);

	bool x=searchchar(arr,ch,size);
	if(x==true){
		printf("charcter found\n");
	}else{
		printf("character not fount\n");
	}
}
