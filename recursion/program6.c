//sum of 1st N natural number using recursion
#include <stdio.h>

int sum(int N){
	if(N == 1){
		return 1;
	}
	return sum(N-1)+N;
}

void main(){

	printf("SUM IS %d\n",sum(5));
}
