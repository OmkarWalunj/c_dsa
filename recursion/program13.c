// palindrome string in character array using recursion

#include <stdio.h>
#include<stdbool.h>

bool ispalindrome(char*arr,int start,int end){

	if(start>=end){
		return true;
	}
	if(arr[end]== arr[start]){
		return ispalindrome(arr,start+1,end-1);
	}else{
		return false;
	}
}

void main(){

	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);
	char arr[size];
	printf("Enter string:\n");
	scanf("%s",arr);
	bool x=ispalindrome(arr,0,size-1);
	if(x==true){
		printf("Is Palindrome\n");
	}else{
		printf("Is not Palindrome\n");
	}
}
