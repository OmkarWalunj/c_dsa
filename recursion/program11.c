// fibonasi series using recursion

#include <stdio.h>

int fibo(int N){

	if(N==1){
		return 1;
	}
	if(N==0){
		return 0;
	}

	return fibo(N-1)+fibo(N-2);
}

void main(){

	int num;
	printf("Enter Number:\n");
	scanf("%d",&num);

	printf("Fibo ans of %d is %d\n",num,fibo(num));
}
