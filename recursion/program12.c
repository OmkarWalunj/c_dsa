// sum of array using recursion

#include <stdio.h>

int sumarr(int*arr,int N){

	if(N==1){
		return arr[N-1];
	}

	return sumarr(arr,N-1)+arr[N-1];
}

void main(){

	int size;
	printf("Enter array size:\n");
	scanf("%d",&size);
	int arr[size];
	printf("Enter Array elements:\n");
	for ( int i=0;i<size;i++){
		scanf("%d",&arr[i]);
	}
	printf("sum of array is %d\n",sumarr(arr,size));
}
