// Implements two stack by using array;

#include<stdio.h>
int top1 =-1;
int top2 =0;
int size=0;
int flag=0;
int push(int * stack,int flag1){
	if(top1==top2-1){
		return -1;
	}else{
		if(flag1==top1){
			printf("Enter data:stack1:\n");
			scanf("%d",&stack[++top1]);
		}else{
			printf("Enter data:stack2:\n");
			scanf("%d",&stack[--top2]);
		}
		return 0;
	}
}
int pop(int *stack,int flag1){
	if(flag1==-1 || flag1==size ){
		flag=0;
		return -1;
	}else{
		flag=1;
		if(flag1==top1){
			return stack[top1--];
		}else{
			return stack[top2++];
		}
	}
}
int peek(int *stack,int flag1){
	if(flag1 ==-1 || flag1==size){
		flag=0;
		return -1;
	}else{
		flag=1;
		if(flag1==top1){
			return stack[top1];
		}else{
			return stack[top2];
		}
	}
}
void main(){
	printf("Enter stack size:\n");
	scanf("%d",&size);

	int stack[size];
	top2=size;
	char choice;

	do{
		printf(" 1.push1\n 2.push2\n 3.pop1\n 4.pop2\n 5.peek1\n 6.peek2\n");
		int ch;
		printf("Enter your choice :\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:{
				int ret=push(stack,top1);
				if(ret==-1)
					printf("stack1 overflow\n");
			       }
				break;
			case 2:
				{
                                int ret=push(stack,top2);
                                if(ret==-1)
                                        printf("stack2 overflow\n");
                               }
                                break;

			case 3:
				{
					int data=pop(stack,top1);
					if(flag==0)
						printf("Stack1 Underflow\n");
					else
						printf("%d is poped:stack1\n ",data);
					
				}
				break;
			case 4:{
                                        int data=pop(stack,top2);
                                        if(flag==0)
                                                printf("Stack2 Underflow\n");
                                        else
                                                printf("%d is poped:stack2\n ",data);
                                        
                                }
                                break;

			case 5:
				{
					int data=peek(stack,top1);
					if(flag ==0)
						printf("Stack1 Underflow\n");
					else
						printf("%d is peek:stack1\n",data);
					
				}
				break;
			  case 6: 
                                {
                                        int data=peek(stack,top2);
                                        if(flag ==0)
                                                printf("Stack2 Underflow\n");
                                        else
                                                printf("%d is peek:stack2\n",data);
                                        
                                }
                                break;

			default :
				printf("Wrong Choice\n");
				break;
		}
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&choice);
	}while((choice=='Y') || (choice == 'y'));
}


