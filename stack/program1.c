// Implements stack by using array;

#include<stdio.h>
int top =-1;
int size=0;
int flag=0;
int push(int * stack){
	if(top==size-1){
		printf("Stack overflow\n");
		return -1;
	}else{
		printf("Enter data:\n");
		scanf("%d",&stack[++top]);
		return 0;
	}
}
int pop(int *stack){
	if(top==-1){
		flag=0;
		printf("Stack underFlow\n");
		return -1;
	}else{
		flag=1;
		int data=stack[top];
		top--;
		return data;
	}
}
int peek(int *stack){
	if(top==-1){
		flag=0;
		printf("Invalid peek\n");
		return -1;
	}else{
		flag=1;
		return stack[top];
	}
}
void main(){
	printf("Enter stack size:\n");
	scanf("%d",&size);

	int stack[size];
	char choice;

	do{
		printf(" 1.push\n 2.pop\n 3.peek\n");
		int ch;
		printf("Enter your choice :\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				push(stack);
				break;
			case 2:
				{
					int data=pop(stack);
					if(flag==1){
						printf("%d is poped\n ",data);
					}
				}
				break;
			case 3:
				{
					int data=peek(stack);
					if(flag !=0){
						printf("%d is peek\n",data);
					}
				}
				break;
			default :
				printf("Wrong Choice\n");
				break;
		}
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&choice);
	}while((choice=='Y') || (choice == 'y'));
}


