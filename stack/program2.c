// Implements stack by using linked list;

#include<stdio.h>
#include <stdlib.h>

typedef struct stack{
	int data;
	struct stack *next;
}stack;

stack *head=NULL;
stack *top=NULL;
int countnode=0;
int flag=0;
int countstack(){
	stack *temp=head;
	int count=0;
	while(temp !=NULL){
		count++;
		temp=temp->next;
	}
	return count;
}
stack *createnode(){
	stack * stacknode =(stack *)malloc(sizeof(stack));

	printf("Enter Data :\n");
	scanf("%d",&stacknode->data);

	stacknode ->next=NULL;
	return stacknode;
}
int push(){
	int count =countstack();
	if(count>=countnode){
		printf("Stack overflow\n");
		return -1;
	}else{
		stack * stacknode=createnode();

		if(head==NULL){
			head=stacknode;
			top=head;
		}else{
			stack *temp=head;
			while(temp->next != NULL){
				temp=temp->next;
			}
			temp->next=stacknode;
			top=temp;
		}

		return 0;
	}
}

int pop(){
	if(head ==NULL){
		flag=0;
		printf("Stack underFlow\n");
		return -1;
	}else{
		flag=1;
		if(head->next==NULL){
			int data=head->data;
			free(head);
			head=NULL;
			return data;
		}else{
			stack *temp=head;
			while(temp->next->next !=NULL){
				temp=temp->next;
			}
			int data= temp->next->data;
			free(temp->next);
			temp->next=NULL;
			top=temp;
			return data;
		}
		
	}
}
int peek(){
	if(head==NULL){
		flag=0;
		printf("Invalid peek\n");
		return -1;
	}else{
		flag=1;
		return top->data;
	}
}
void main(){
	printf("Enter stack size:\n");
	scanf("%d",&countnode);

	char choice;

	do{
		printf(" 1.push\n 2.pop\n 3.peek\n");
		int ch;
		printf("Enter your ch2oice :\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				push();
				break;
			case 2:
				{
					int data=pop();
					if(flag==1){
						printf("%d is poped\n ",data);
					}
				}
				break;
			case 3:
				{
					int data=peek();
					if(flag !=0){
						printf("%d is peek\n",data);
					}
				}
				break;
			default :
				printf("Wrong Choice\n");
				break;
		}
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&choice);
	}while((choice=='Y') || (choice == 'y'));
}


